var validator = require("validator");

function emptyValidate(value, idError) {
  if (validator.isEmpty(value)) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "Không được để rỗng";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

// tai khoan
function idRangeValidate(value, idError) {
  if (!validator.isLength(value, { min: 4, max: 6 })) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "Tài khoản từ 4 - 6 ký số!";
    return false;
  } else {
    document.getElementById(idError).style.display = "none";
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function duplicateValidate(idNv, listNv, idError) {
  var index = listNv.findIndex(function (nv) {
    return nv.account == idNv;
  });

  if (index == -1) {
    document.getElementById(idError).style.display = "none";
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText =
      "Tài khoản nhân viên đã tồn tại";
    return false;
  }
}

// ten nhan vien
function nameValidate(value, idError) {
  if (!validator.isAlpha(value)) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "Tên nhân viên phải là chữ!!!";
    return false;
  } else {
    document.getElementById(idError).style.display = "none";
    document.getElementById(idError).innerText = "";
    return true;
  }
}

// email
function emailValidate(value, idError) {
  if (!validator.isEmail(value)) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "Nhập đúng định dạng email !!";
    return false;
  } else {
    document.getElementById(idError).style.display = "none";
    document.getElementById(idError).innerText = "";
    return true;
  }
}

// password
function passwordValidate(value, idError) {
  var isValid =
    validator.isLength(value, { min: 6, max: 10 }) &&
    validator.isStrongPassword(value, {
      minLength: 6,
      minUppercase: 1,
      minNumbers: 1,
      minSymbols: 1,
    });
  if (!isValid) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText =
      "Mật khẩu từ 6 - 10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt) !!";
    return false;
  } else {
    document.getElementById(idError).style.display = "none";
    document.getElementById(idError).innerText = "";
    return true;
  }
}

// ngay lam
function dateValidate(value, idError) {
  if (!validator.isDate(value, "MM/DD/YYYY")) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "Định dạng ngày là MM/DD/YYYY";
    return false;
  } else {
    document.getElementById(idError).style.display = "none";
    document.getElementById(idError).innerText = "";
    return true;
  }
}

// luong
function luongValidate(value, idError) {
  if (!validator.isInt(value, { min: 1000000, max: 20000000 })) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText =
      "Lương cơ bản 1 000 000 - 20 000 000";
    return false;
  } else {
    document.getElementById(idError).style.display = "none";
    document.getElementById(idError).innerText = "";
    return true;
  }
}

// chuc vu
function chucvuValidate(value, idError) {
  if (value == 0) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "Vui lòng chọn chức vụ hợp lệ";
    return false;
  } else {
    document.getElementById(idError).style.display = "none";
    document.getElementById(idError).innerText = "";
    return true;
  }
}

// so gio lam
function hourValidate(value, idError) {
  if (!validator.isInt(value, { min: 80, max: 200 })) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText =
      "Số giờ làm trong tháng 80 - 200 giờ";
    return false;
  } else {
    document.getElementById(idError).style.display = "none";
    document.getElementById(idError).innerText = "";
    return true;
  }
}
