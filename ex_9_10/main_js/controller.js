function layThongTinTuForm() {
  // lấy thông tin từ user
  var account = document.getElementById("tknv").value.trim();
  var name = document.getElementById("name").value.trim();
  var email = document.getElementById("email").value.trim();
  var password = document.getElementById("password").value.trim();
  var datepicker = document.getElementById("datepicker").value.trim();
  var luongCB = document.getElementById("luongCB").value.trim();
  var chucvu = document.getElementById("chucvu").value.trim();
  var giolam = document.getElementById("gioLam").value.trim();

  // console.log(luongCB);

  // tạo object từ thông tin lấy từ form
  var nv = new Staff(
    account,
    name,
    email,
    password,
    datepicker,
    luongCB,
    chucvu,
    giolam
  );
  return nv;
}

function renderDsnv(list) {
  // render danh sách
  // contentHTML là 1 chuổi chứa các thẻ tr sau này sẽ innerHTML vào thẻ tbody
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    var currentNv = list[i];
    var contentTr = `<tr> 
      <td>${currentNv.account}</td>
      <td>${currentNv.name}</td>
      <td>${currentNv.email}</td>
      <td>${currentNv.datepick}</td>
      <td>${currentNv.chucvu}</td>
      <td>${currentNv.tongLuong()}</td>
      <td>${currentNv.loaiNhanvien()}</td>
      <td>
        <button onclick="xoaNv('${
          currentNv.account
        }')"  class="btn btn-danger">Xoá</button>

        <button onclick="suaNv('${currentNv.account}')"
        class="btn btn-primary">Sửa</button>
      </td>
    </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.account;
  document.getElementById("name").value = nv.name;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.password;
  document.getElementById("datepicker").value = nv.datepick;
  document.getElementById("luongCB").value = nv.luongcb;
  document.getElementById("chucvu").value = nv.chucvu;
  document.getElementById("gioLam").value = nv.giolam;
}

function resetForm() {
  // reset form
  document.getElementById("formQLNV").reset();
}
