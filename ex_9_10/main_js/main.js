const DSNV = "DSNV";
var dsnv = [];
// lấy dữ liệu lên từ localStorage
var dataJson = localStorage.getItem(DSNV);
if (dataJson) {
  //truthy falsy
  var dataRaw = JSON.parse(dataJson);
  dsnv = dataRaw.map(function (item) {
    return new Staff(
      item.account,
      item.name,
      item.email,
      item.password,
      item.datepick,
      item.luongcb,
      item.chucvu,
      item.giolam
    );
  });

  renderDsnv(dsnv);
}

function saveLocalStorage() {
  // lưu xuống local
  var dsnvJson = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, dsnvJson);
}

function themNv() {
  var newNv = layThongTinTuForm();
  var isValid = true;

  // kiem tra tai khoan nhan vien
  isValid &=
    emptyValidate(newNv.account, "tbTKNV") &&
    idRangeValidate(newNv.account, "tbTKNV") &&
    duplicateValidate(newNv.account, dsnv, "tbTKNV");

  // kiem tra ten
  isValid &=
    emptyValidate(newNv.name, "tbTen") && nameValidate(newNv.name, "tbTen");

  // kiem tra email
  isValid &=
    emptyValidate(newNv.email, "tbEmail") &&
    emailValidate(newNv.email, "tbEmail");

  // kiem tra mat khau
  isValid &=
    emptyValidate(newNv.password, "tbMatKhau") &&
    passwordValidate(newNv.password, "tbMatKhau");

  // kiem tra ngay lam
  isValid &=
    emptyValidate(newNv.datepick, "tbNgay") &&
    dateValidate(newNv.datepick, "tbNgay");

  // kiem tra luong co ban
  isValid &=
    emptyValidate(newNv.luongcb, "tbLuongCB") &&
    luongValidate(newNv.luongcb, "tbLuongCB");

  // kiem tra chuc vu
  isValid &= chucvuValidate(newNv.chucvu, "tbChucVu");

  // kiem tra gio lam
  isValid &=
    emptyValidate(newNv.giolam, "tbGiolam") &&
    hourValidate(newNv.giolam, "tbGiolam");

  // console.log("ketqua: ", isValid);
  if (isValid) {
    dsnv.push(newNv);
    saveLocalStorage();
    renderDsnv(dsnv);
    resetForm();
  }
}

function xoaNv(idNv) {
  var index = dsnv.findIndex(function (nv) {
    return nv.account == idNv;
  });
  if (index == -1) {
    return;
  }
  //  xoá phần tử khỏi danh sách
  dsnv.splice(index, 1);

  //  sau khi xoá thì dữ liệu thay đổi , nhưng layout ko có thay đổi vì layout được tạo ra nhờ renderDsnv
  renderDsnv(dsnv);

  // update dữ liệu cho localStorage

  saveLocalStorage();
}

function suaNv(idNv) {
  // console.log(1);
  // console.log(idNv);
  var index = dsnv.findIndex(function (nv) {
    return nv.account == idNv;
  });
  if (index == -1) return;

  var nv = dsnv[index];
  // console.log("nv: ", nv);
  document.getElementById("btnThem").click();
  showThongTinLenForm(nv);

  // disable mã nv
  document.getElementById("tknv").disabled = true;
}

function capNhatNv() {
  var nvEdit = layThongTinTuForm();

  var index = dsnv.findIndex(function (nv) {
    return nv.account == nvEdit.account;
  });

  // console.log("nhan vien account: ", nvEdit.account);
  // console.log("index of account: ", index);
  if (index == -1) return;

  dsnv[index] = nvEdit;
  // console.log(dsnv[index]);
  saveLocalStorage();
  renderDsnv(dsnv);
  resetForm();

  document.getElementById("tknv").disabled = false;
  document.getElementById("btnDong").click();
}

// filter
function filterbyPerformance() {
  var performanceValue = document.getElementById("filterbyPerformance").value;
  var contentHTML = "";
  for (var i = 0; i < dsnv.length; i++) {
    var currentNv = dsnv[i];
    if (performanceValue == currentNv.loaiNhanvien()) {
      var contentTr = `<tr> 
        <td>${currentNv.account}</td>
        <td>${currentNv.name}</td>
        <td>${currentNv.email}</td>
        <td>${currentNv.datepick}</td>
        <td>${currentNv.chucvu}</td>
        <td>${currentNv.tongLuong()}</td>
        <td>${currentNv.loaiNhanvien()}</td>
        <td>
          <button onclick="xoaNv('${
            currentNv.account
          }')"  class="btn btn-danger">Xoá</button>

          <button onclick="suaNv('${currentNv.account}')"
          class="btn btn-primary">Sửa</button>
        </td>
      </tr>`;
      contentHTML += contentTr;
    }
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
  if (performanceValue == "0") {
    renderDsnv(dsnv);
  }
}
