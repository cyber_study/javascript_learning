function Staff(
  _account,
  _name,
  _email,
  _password,
  _datepick,
  _luongcb,
  _chucvu,
  _giolam
) {
  this.account = _account;
  this.name = _name;
  this.email = _email;
  this.password = _password;
  this.datepick = _datepick;
  this.luongcb = _luongcb;
  this.chucvu = _chucvu;
  this.giolam = _giolam;

  this.tongLuong = function () {
    switch (this.chucvu) {
      case "Sếp":
        return this.luongcb * 1 * 3;
      case "Trưởng phòng":
        return this.luongcb * 1 * 2;
      case "Nhân viên":
        return this.luongcb * 1 * 1;
    }
  };

  this.loaiNhanvien = function () {
    var loaiNv = this.giolam * 1;
    if (loaiNv >= 192) {
      return "Xuất sắc";
    } else if (loaiNv >= 176) {
      return "Giỏi";
    } else if (loaiNv >= 160) {
      return "Khá";
    } else {
      return "Trung bình";
    }
  };
}
