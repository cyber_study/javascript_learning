// bai 1
function tax() {
  // get date
  var incomeValue = document.getElementById("income").value * 1;
  var ppDependValue = document.getElementById("ppDepend").value * 1;

  const PERCENT = [0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35];
  // calculate taxable value
  var taxableValue = incomeValue - 4 - ppDependValue * 1.6;

  // create varialbes
  var taxValue = 0;
  var caseBill = 0;

  if (taxableValue <= 60) {
    caseBill = 1;
  } else if (taxableValue > 60 && taxableValue <= 120) {
    caseBill = 2;
  } else if (taxableValue > 120 && taxableValue <= 210) {
    caseBill = 3;
  } else if (taxableValue > 210 && taxableValue <= 284) {
    caseBill = 4;
  } else if (taxableValue > 284 && taxableValue <= 384) {
    caseBill = 5;
  } else if (taxableValue > 384 && taxableValue <= 624) {
    caseBill = 6;
  } else if (taxableValue > 624 && taxableValue <= 960) {
    caseBill = 7;
  } else {
    caseBill = 8;
  }

  switch (caseBill) {
    case 1: {
      taxValue = taxableValue * PERCENT[caseBill - 1];

      break;
    }
    case 2: {
      taxValue = taxableValue * PERCENT[caseBill - 1];
      break;
    }
    case 3: {
      taxValue = taxableValue * PERCENT[caseBill - 1];
      break;
    }
    case 4: {
      taxValue = taxableValue * PERCENT[caseBill - 1];
      break;
    }
    case 5: {
      taxValue = taxableValue * PERCENT[caseBill - 1];
      break;
    }
    case 6: {
      taxValue = taxableValue * PERCENT[caseBill - 1];
      break;
    }
    case 7: {
      taxValue = taxableValue * PERCENT[caseBill - 1];
      break;
    }
    case 8: {
      taxValue = taxableValue * PERCENT[caseBill - 1];
      break;
    }
  }

  document.getElementById("result_1").innerHTML = `
  <p>Thuế phải nộp của anh/chị là ${Math.round(taxValue * 1000000)} vnđ</p>
  `;
}

// bai 2

function toggler() {
  var customerTypeValue = document.getElementById("customerType").value * 1;
  const TOOGLER = document.getElementById("toogleByCustomer").classList;

  if (customerTypeValue == 0) {
    TOOGLER.add("d-none");
  } else {
    TOOGLER.remove("d-none");
  }
}

function tinhTienCap() {
  var customerIdValue = document.getElementById("customerId").value;
  var customerTypeValue = document.getElementById("customerType").value * 1;
  var connectionsValue = document.getElementById("connections").value * 1;
  var payChannelValue = document.getElementById("payChannel").value * 1;

  var result_2 = 0;

  switch (customerTypeValue) {
    case 0: {
      result_2 = 4.5 + 20.5 + 7.5 * payChannelValue;
      break;
    }
    case 1: {
      if (connectionsValue <= 10) {
        result_2 = 15 + 75 + 50 * payChannelValue;
      } else {
        result_2 = 15 + 75 + (connectionsValue - 10) * 5 + 50 * payChannelValue;
        break;
      }
    }
  }

  document.getElementById("result_2").innerHTML = `
  <p>Hóa đơn của khách hàng ${customerIdValue}  là ${result_2}$</p>
  `;
}
