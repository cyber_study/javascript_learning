// bai 1
function bai1() {
  var num1 = document.getElementById("b1_num1").value * 1;
  var num2 = document.getElementById("b1_num2").value * 1;
  var num3 = document.getElementById("b1_num3").value * 1;

  const ARRAY_BAI1 = [num1, num2, num3];
  ARRAY_BAI1.sort(function (a, b) {
    return a - b;
  });

  document.getElementById("result_1").innerHTML = `
    <p>Kết quả:  ${ARRAY_BAI1}</p>
  `;
  // console.log(ARRAY_BAI1);
}

// bai2
function bai2() {
  var member = document.querySelector(
    'input[name="greetingInput"]:checked'
  ).value;

  console.log(member);

  switch (member) {
    case "A":
      document.getElementById("result_2").innerHTML = `
        <p>Xin chào anh trai</p>
      `;
      break;
    case "B":
      document.getElementById("result_2").innerHTML = `
        <p>Xin chào bố</p>
      `;
      break;
    case "E":
      document.getElementById("result_2").innerHTML = `
        <p>Xin chào em gái</p>
      `;
      break;
    case "M":
      document.getElementById("result_2").innerHTML = `
        <p>Xin chào mẹ</p>
      `;
      break;
  }

  // console.log(member);
}

// bai 3
function bai3() {
  var countOddNum = 0;
  var countEvenNum = 0;

  var num1 = document.getElementById("b3_num1").value * 1;
  var num2 = document.getElementById("b3_num2").value * 1;
  var num3 = document.getElementById("b3_num3").value * 1;

  if (num1 % 2 == 0) {
    countEvenNum += 1;
  } else countOddNum += 1;

  if (num2 % 2 == 0) {
    countEvenNum += 1;
  } else countOddNum += 1;

  if (num3 % 2 == 0) {
    countEvenNum += 1;
  } else countOddNum += 1;

  document.getElementById("result_3").innerHTML = `
    <p>Có ${countEvenNum} số chẵn và ${countOddNum} số lẻ</p>
  `;
}

function bai4() {
  var canh1 = document.getElementById("canh1").value * 1;
  var canh2 = document.getElementById("canh2").value * 1;
  var canh3 = document.getElementById("canh3").value * 1;

  const ARRAY_BAI4 = [canh1, canh2, canh3];
  ARRAY_BAI4.sort(function (a, b) {
    return a - b;
  });

  if (ARRAY_BAI4[0] + ARRAY_BAI4[1] <= ARRAY_BAI4[2]) {
    return (document.getElementById("result_4").innerHTML = `
    <p>Không phải tam giác</p>
  `);
  }

  // figure triangle
  if (canh1 == canh2 && canh2 == canh3) {
    return (document.getElementById("result_4").innerHTML = `
    <p>Tam giác đều</p>
  `);
  } else if (
    ARRAY_BAI4[0] * ARRAY_BAI4[0] + ARRAY_BAI4[1] * ARRAY_BAI4[1] ==
    ARRAY_BAI4[2] * ARRAY_BAI4[2]
  ) {
    document.getElementById("result_4").innerHTML = `
    <p>Tam giác vuông</p>
  `;
  } else if (
    ARRAY_BAI4[0] == ARRAY_BAI4[1] ||
    ARRAY_BAI4[0] == ARRAY_BAI4[2] ||
    ARRAY_BAI4[1] == ARRAY_BAI4[2]
  ) {
    return (document.getElementById("result_4").innerHTML = `
    <p>Tam giác cân</p>
  `);
  } else {
    return (document.getElementById("result_4").innerHTML = `
    <p>Tam giác thường</p>
  `);
  }
}
