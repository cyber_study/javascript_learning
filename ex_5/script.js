function bai1() {
  var benchmarkValue = document.getElementById("benchmark").value * 1;

  var areaValue = document.getElementById("area").value * 1;
  var subjectValue = document.getElementById("subject").value * 1;

  var pointX = document.getElementById("pointX").value * 1;
  var pointY = document.getElementById("pointY").value * 1;
  var pointZ = document.getElementById("pointZ").value * 1;

  var candidatePoint = pointX + pointY + pointZ + areaValue + subjectValue;

  if (pointX != 0 && pointY != 0 && pointZ != 0) {
    if (candidatePoint >= benchmarkValue) {
      document.getElementById("result_1").innerHTML = `
      <p>Chúc mừng bạn đã trúng tuyển</p>
    `;
    } else {
      document.getElementById("result_1").innerHTML = `
      <p>Thất bại là mẹ thành công</p>
    `;
    }
  } else {
    document.getElementById("result_1").innerHTML = `
      <p>Cố gắng đừng để liệt nữa nhé!</p>
    `;
  }
}

function bai2() {
  var customerName = document.getElementById("customerName").value;
  var killowattValue = document.getElementById("killowatt").value * 1;

  const PRICE = [500, 650, 850, 1100, 1300];
  var payment_2 = 0;
  var caseBill;

  if (killowattValue <= 50) {
    caseBill = 1;
  } else if (killowattValue > 50 && killowattValue <= 100) {
    caseBill = 2;
  } else if (killowattValue > 100 && killowattValue <= 200) {
    caseBill = 3;
  } else if (killowattValue > 200 && killowattValue <= 350) {
    caseBill = 4;
  } else {
    caseBill = 5;
  }

  switch (caseBill) {
    case 1: {
      payment_2 = killowattValue * PRICE[0];
      break;
    }
    case 2: {
      payment_2 = 50 * PRICE[0] + (killowattValue - 50) * PRICE[1];
      break;
    }
    case 3: {
      payment_2 =
        50 * PRICE[0] + 50 * PRICE[1] + (killowattValue - 100) * PRICE[2];
      break;
    }
    case 4: {
      payment_2 =
        50 * PRICE[0] +
        50 * PRICE[1] +
        100 * PRICE[2] +
        (killowattValue - 200) * PRICE[3];
      break;
    }
    case 5: {
      payment_2 =
        50 * PRICE[0] +
        50 * PRICE[1] +
        100 * PRICE[2] +
        150 * PRICE[3] +
        (killowattValue - 350) * PRICE[4];
      break;
    }
  }

  document.getElementById("result_2").innerHTML = `
  <p>Hóa đơn của anh/chị ${customerName} là ${payment_2} vnđ</p>
  `;
}
