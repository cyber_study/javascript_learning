// start bai 1
var result_1 = 0;
var condition_1 = 0;

while (condition_1 <= 1000) {
  result_1++;
  condition_1 += result_1;
}

document.getElementById("result_1").innerHTML = `
  <p>Kết quả bài 1 là : ${result_1}.</p>
`;
// end bai 1

// start bai 2
function bai2() {
  var result_2 = 0;
  var numX = document.getElementById("numX").value * 1;
  var numExponent = document.getElementById("numExponent").value * 1;

  for (var n = 1; n <= numExponent; n++) {
    result_2 += Math.pow(numX, n);
  }

  document.getElementById("result_2").innerHTML = `
  <p>Kết quả bài 2 là : ${result_2}.</p>
`;
}
// end bai 2

// start bai 3
function bai3() {
  var result_3 = 1;
  var factorialNumber = document.getElementById("factorialNumber").value * 1;

  for (var n = 1; n <= factorialNumber; n++) {
    result_3 *= n;
  }

  document.getElementById("result_3").innerHTML = `
  <p>Kết quả bài 3 là : ${result_3}.</p>
`;
}
// end bai 3

// start bai 4
function bai4() {
  var content = "";

  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      content += `
        <div class="bg-danger py-1 px-1 mb-1">chẵn ${i}</div>
      `;
    } else {
      content += `
        <div class="bg-primary py-1 px-1 mb-1">lẻ ${i}</div>
      `;
    }
  }

  document.getElementById("content").innerHTML = content;
}
// end bai 4

// start bai 5
function bai5() {
  var primeNumber = document.getElementById("primeNumber").value * 1;

  var content = "";

  function test_prime(n) {
    if (n == 1) {
      return content;
    } else if (n == 2) {
      return (content += n + " ");
    } else {
      for (var x = 2; x < n; x++) {
        if (n % x == 0) {
          return content;
        }
      }
      return (content += n + " ");
    }
  }

  for (var i = 1; i <= primeNumber; i++) {
    test_prime(i);
  }

  document.getElementById("result_5").innerHTML = content;
}
// end bai 5
