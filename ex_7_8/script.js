const DISPLAY_ARRAY = [];

function addNum() {
  var inputNumberValue = document.getElementById("inputNumber").value * 1;

  DISPLAY_ARRAY.push(inputNumberValue);

  var contentArray = "";
  for (var i = 0; i < DISPLAY_ARRAY.length; i++) {
    contentArray += `${DISPLAY_ARRAY[i]},`;
    document.getElementById(
      "displayArray"
    ).innerHTML = `<p>${contentArray}</p>`;
  }

  // console.log(DISPLAY_ARRAY);
}

function sumPos() {
  var result = 0;
  for (var i = 0; i < DISPLAY_ARRAY.length; i++) {
    if (DISPLAY_ARRAY[i] > 0) {
      result += DISPLAY_ARRAY[i];
    }
  }

  document.querySelector(
    "#sumPositive #result"
  ).innerHTML = `<span>${result}</span>`;

  // document.getElementById("result_1").innerHTML = `${result}`;
}

function countPos() {
  var result = 0;
  for (var i = 0; i < DISPLAY_ARRAY.length; i++) {
    if (DISPLAY_ARRAY[i] > 0) {
      result += 1;
    }
  }

  document.querySelector(
    "#countPositive #result"
  ).innerHTML = `<span>${result}</span>`;
}

function findMin() {
  var result = DISPLAY_ARRAY[0];

  for (var i = 1; i < DISPLAY_ARRAY.length; i++) {
    if (result < DISPLAY_ARRAY[i]) {
      result = DISPLAY_ARRAY[i];
    }
  }

  document.querySelector(
    "#findMin #result"
  ).innerHTML = `<span>${result}</span>`;
}

function findMin() {
  var result = DISPLAY_ARRAY[0];

  for (var i = 1; i < DISPLAY_ARRAY.length; i++) {
    if (result > DISPLAY_ARRAY[i]) {
      result = DISPLAY_ARRAY[i];
    }
  }

  document.querySelector(
    "#findMin #result"
  ).innerHTML = `<span>${result}</span>`;
}

function findMinPos() {
  var result = DISPLAY_ARRAY[0];

  for (var i = 1; i < DISPLAY_ARRAY.length; i++) {
    if (result > DISPLAY_ARRAY[i] && DISPLAY_ARRAY[i] > 0) {
      result = DISPLAY_ARRAY[i];
    }
  }

  if (result < 0) {
    document.querySelector(
      "#findMinPos #result"
    ).innerHTML = `<span>Chuỗi không có số dương</span>`;
  } else {
    document.querySelector(
      "#findMinPos #result"
    ).innerHTML = `<span>${result}</span>`;
  }
}

function lastEven() {
  for (var i = DISPLAY_ARRAY.length - 1; i >= 0; i--) {
    if (DISPLAY_ARRAY[i] % 2 == 0) {
      return (document.querySelector(
        "#lastEven #result"
      ).innerHTML = `<span>${DISPLAY_ARRAY[i]}
      </span>`);
    }
  }
  return (document.querySelector(
    "#lastEven #result"
  ).innerHTML = `<span>-1</span>`);
}

function changePosition() {
  var index1Value = document.getElementById("index1").value * 1 - 1;
  var index2Value = document.getElementById("index2").value * 1 - 1;

  var changedArray = DISPLAY_ARRAY;
  var storingValue = changedArray[index1Value];
  changedArray[index1Value] = changedArray[index2Value];
  changedArray[index2Value] = storingValue;

  document.querySelector(
    "#changePosition #result"
  ).innerHTML = `<span>${changedArray}</span>`;
}

function sortIncrease() {
  var sortArray = DISPLAY_ARRAY;

  sortArray.sort(function (a, b) {
    return a - b;
  });

  document.querySelector(
    "#sortIncrease #result"
  ).innerHTML = `<span>${sortArray}</span>`;
}

function firstPrimeNum() {
  for (var i = 0; i < DISPLAY_ARRAY.length; i++) {
    if (DISPLAY_ARRAY[i] < 1) {
      continue;
    }
    if (DISPLAY_ARRAY[i] == 2) {
      return (document.querySelector(
        "#firstPrimeNum #result"
      ).innerHTML = `<span>${DISPLAY_ARRAY[i]}</span>`);
    } else {
      for (var j = 2; j < DISPLAY_ARRAY[i]; j++) {
        if (DISPLAY_ARRAY[i] % j == 0) {
          break;
        }
        return (document.querySelector(
          "#firstPrimeNum #result"
        ).innerHTML = `<span>${DISPLAY_ARRAY[i]}</span>`);
      }
    }
  }

  return (document.querySelector(
    "#firstPrimeNum #result"
  ).innerHTML = `<span>-1</span>`);
}

function countInt() {
  var result = 0;
  for (var i = 0; i < DISPLAY_ARRAY.length; i++) {
    if (Number.isInteger(DISPLAY_ARRAY[i])) {
      result++;
    }
  }

  return (document.querySelector(
    "#countInt #result"
  ).innerHTML = `<span>Có ${result} số nguyên</span>`);
}

function comparePosandNev() {
  var nevNumber = 0;
  var posNumber = 0;
  for (var i = 0; i < DISPLAY_ARRAY.length; i++) {
    if (DISPLAY_ARRAY[i] == 0) {
      continue;
    } else if (DISPLAY_ARRAY[i] < 0) {
      nevNumber++;
    } else {
      posNumber++;
    }
  }

  if (nevNumber > posNumber) {
    return (document.querySelector(
      "#comparePosandNev #result"
    ).innerHTML = `<span>Số âm > Số dương</span>`);
  } else if (nevNumber == posNumber) {
    return (document.querySelector(
      "#comparePosandNev #result"
    ).innerHTML = `<span>Số âm = Số dương</span>`);
  } else {
    return (document.querySelector(
      "#comparePosandNev #result"
    ).innerHTML = `<span>Số âm < Số dương</span>`);
  }
}
