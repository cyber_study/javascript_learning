function bai1() {
  var b1_date = document.getElementById("b1_date").value;
  var b1_month = document.getElementById("b1_month").value;
  var b1_year = document.getElementById("b1_year").value;

  const CURRENT_DATE = new Date(b1_year, b1_month - 1, b1_date);

  var next_day = new Date();
  next_day.setDate(CURRENT_DATE.getDate() + 1);
  var prev_day = new Date();
  prev_day.setDate(CURRENT_DATE.getDate() - 1);

  document.getElementById("result_1").innerHTML = `
    <p>Ngày tiếp theo: ${next_day}</p>
    <p>Ngày hôm trước: ${prev_day}</p>    
  `;
}

function bai2() {
  var b2_month = document.getElementById("b2_month").value;
  var b2_year = document.getElementById("b2_year").value;

  var feb_day = 0;
  var result_2 = 0;

  if (b2_year % 4 == 0) {
    feb_day = 1;
  }

  switch (b2_month) {
    case "2":
      if (feb_day == 1) {
        result_2 = 28 + 1;
      } else result_2 = 28;
      break;
    case "4":
    case "6":
    case "9":
    case "11":
      result_2 = 30;
      break;
    default:
      result_2 = 31;
      break;
  }

  document.getElementById("result_2").innerHTML = `
    <p>Tháng này có ${result_2} ngày</p> 
  `;
}

function bai3() {
  var readNum = document.getElementById("readNum").value;

  if (readNum[1] == "0") {
    document.getElementById("result_3").innerHTML = `
    <p>${readNum[0]} trăm lẻ ${readNum[2]}</p> 
  `;
  } else if (readNum[1] == "1") {
    document.getElementById("result_3").innerHTML = `
    <p>${readNum[0]} trăm mười ${readNum[2]}</p> 
  `;
  } else {
    document.getElementById("result_3").innerHTML = `
    <p>${readNum[0]} trăm ${readNum[0]} mươi ${readNum[2]}</p> 
  `;
  }
}

function bai4() {
  var student_name_1 = document.getElementById("student_name_1").value;
  var student_name_2 = document.getElementById("student_name_2").value;
  var student_name_3 = document.getElementById("student_name_3").value;

  var cordi_x_1 = document.getElementById("cordi_x_1").value * 1;
  var cordi_y_1 = document.getElementById("cordi_y_1").value * 1;

  var distance_1 = cordi_x_1 * cordi_x_1 + cordi_y_1 * cordi_y_1;

  var cordi_x_2 = document.getElementById("cordi_x_2").value * 1;
  var cordi_y_2 = document.getElementById("cordi_y_2").value * 1;
  var distance_2 = cordi_x_2 * cordi_x_2 + cordi_y_2 * cordi_y_2;

  var cordi_x_3 = document.getElementById("cordi_x_3").value * 1;
  var cordi_y_3 = document.getElementById("cordi_y_3").value * 1;
  var distance_3 = cordi_x_3 * cordi_x_3 + cordi_y_3 * cordi_y_3;

  if (distance_1 > distance_2 && distance_1 > distance_3) {
    document.getElementById("result_4").innerHTML = `
    <p>Sinh viên ${student_name_1} xa trường nhất</p> 
  `;
  } else if (distance_2 > distance_1 && distance_2 > distance_3) {
    document.getElementById("result_4").innerHTML = `
    <p>Sinh viên ${student_name_2} xa trường nhất</p> 
  `;
  } else {
    document.getElementById("result_4").innerHTML = `
    <p>Sinh viên ${student_name_3} xa trường nhất</p> 
  `;
  }
}
